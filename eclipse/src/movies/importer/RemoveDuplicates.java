//Angela Sposato 1934695
package movies.importer;

import java.util.ArrayList;

public class RemoveDuplicates extends Processor{
	public RemoveDuplicates(String srcDir, String outputDir) {
		super(srcDir, outputDir, false);
	}
	public ArrayList<String> process(ArrayList<String> input){
		ArrayList<String> noDuplicates = new ArrayList<String>();
		for (int i = 0; i<input.size(); i++) {
			if (!noDuplicates.contains(input.get(i))) {
				noDuplicates.add(input.get(i));
			}
		}
		return noDuplicates;
	}
}
