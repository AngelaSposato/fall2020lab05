//Angela Sposato 1934695
package movies.importer;

import java.io.IOException;

public class ProcessingTest {

	public static void main(String[] args) throws IOException{
		String source = "C:\\Users\\angel\\Documents\\School\\Java3\\Lab5";
		String destination ="C:\\Users\\angel\\Documents\\School\\Java3\\Lab5\\destination";
		LowercaseProcessor processor = new LowercaseProcessor(source, destination);
		processor.execute();
		String source1 = "C:\\Users\\angel\\Documents\\School\\Java3\\Lab5\\destination";
		String destination1 ="C:\\Users\\angel\\Documents\\School\\Java3\\Lab5\\destination2";
		RemoveDuplicates noDupes = new RemoveDuplicates(source1, destination1);
		noDupes.execute();
	}

}
